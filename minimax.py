from konstante import *
from potez import *
from stanje_igre import *

def minimax(tabla, dubina, maximizing_player, boja, alpha, beta):
    # Minimax algoritam s alfa-beta rezovima
    svi_validni_potezi = validni_potezi(tabla, boja)

    if dubina == 0 or len(svi_validni_potezi) == 0:
        return stanje(tabla, boja)

    if maximizing_player:
        max_eval = float("-inf")
        for move in svi_validni_potezi:
            nova_tabla = [red[:] for red in tabla]
            odigraj(nova_tabla, move[0], move[1], boja)
            eval = minimax(nova_tabla, dubina - 1, False, boja, alpha, beta)
            max_eval = max(max_eval, eval)
            alpha = max(alpha, eval)
            if beta <= alpha:
                break
        return max_eval
    else:
        min_eval = float("inf")
        for move in svi_validni_potezi:
            nova_tabla = [red[:] for red in tabla]
            odigraj(nova_tabla, move[0], move[1], boja)
            eval = minimax(nova_tabla, dubina - 1, True, boja, alpha, beta)
            min_eval = min(min_eval, eval)
            beta = min(beta, eval)
            if beta <= alpha:
                break
        return min_eval


def najbolji_potez(tabla, dubina, boja):
    #Dobavljanje najboljeg poteza za prosledjenu boju
    najbolji_rezultat = float("-inf")
    najbolji_potez = None
    alpha = float("-inf")
    beta = float("inf")
    svi_validni_potezi = validni_potezi(tabla, boja)

    for move in svi_validni_potezi:
        nova_tabla = [red[:] for red in tabla]
        odigraj(nova_tabla, move[0], move[1], boja)
        rezultat = minimax(nova_tabla, dubina - 1, False, boja, alpha, beta)
        if rezultat > najbolji_rezultat:
            najbolji_rezultat = rezultat
            najbolji_potez = move

    return najbolji_potez

def odredi_dubinu(tabla):
    #Odredjuje dubinu na osnovu praznih polja na tabli
    prazna_polja = sum(red.count(PRAZNO) for red in tabla)

    if prazna_polja >= 40:
        dubina = 3
    elif prazna_polja >= 20:
        dubina = 4
    else:
        dubina = 5

    return dubina