from konstante import *
from tabla import *
from minimax import *
from potez import *

def play():
    tabla = napravi_tablu()
    igrac_na_potezu = BELI

    while True:
        iscrtaj_tablu(tabla)

        if igrac_na_potezu == CRNI:
            print("Crni je na potezu")
            dubina = odredi_dubinu(tabla)
            potez = najbolji_potez(tabla, dubina, CRNI)
            odigraj(tabla, potez[0], potez[1], CRNI)
        else:
            print("Beli na potezu")
            ispisi_validne_poteze(tabla, BELI)
            red = int(input("Unesite red: "))
            kolona = int(input("Unesite kolonu: "))
            odigraj(tabla, red, kolona, BELI)

        igrac_na_potezu = BELI if igrac_na_potezu == CRNI else CRNI

        rezultat_crni = proveri_rezultat(tabla, CRNI)
        rezultat_beli = proveri_rezultat(tabla, BELI)
        if len(validni_potezi(tabla, CRNI)) == 0 and len(validni_potezi(tabla, BELI)) == 0:
            print("KRAJ!")
            if rezultat_crni > rezultat_beli:
                print("crni je pobedio")
            elif rezultat_beli > rezultat_crni:
                print("beli je pobedio")
            else:
                print("nereseno")
            
            print("Konacan rezultat:")
            print("Beli: " + str(rezultat_beli))
            print("Crni: " + str(rezultat_crni))
            break