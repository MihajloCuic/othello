from konstante import *

def validan_potez(tabla, red, kolona, boja):
    # Provera da li je potez validan
    if tabla[red][kolona] != PRAZNO:
        return False

    druga_boja = BELI if boja == CRNI else CRNI

    # Provera susednih polja za suprotnu boju
    for i in range(-1, 2):
        for j in range(-1, 2):
            if i == 0 and j == 0:
                continue
            r, kol = red + i, kolona + j
            if (r >= 0 and r < SIZE and kol >= 0 and kol < SIZE and tabla[r][kol] == druga_boja):
                r += i
                kol += j
                if (r >= 0 and r < SIZE and kol >= 0 and kol < SIZE and tabla[r][kol] == boja):
                    return True

    return False


def odigraj(tabla, red, kolona, boja):
    # Izvrsavanje poteza
    if not validan_potez(tabla, red, kolona, boja):
        print("Ne mozete da odigrate taj potez")

    tabla[red][kolona] = boja
    druga_boja = BELI if boja == CRNI else CRNI

    # Provera svih smerova za protivnicke ploce koje treba preokrenuti
    for i in range(-1, 2):
        for j in range(-1, 2):
            if i == 0 and j == 0:
                continue
            r, kol = red + i, kolona + j
            obrni = []
            while (r >= 0 and r < SIZE and kol >= 0 and kol < SIZE and tabla[r][kol] == druga_boja):
                obrni.append((r, kol))
                r += i
                kol += j
                if (r >= 0 and r < SIZE and kol >= 0 and kol < SIZE and tabla[r][kol] == boja):
                    for obrni_red, obrni_kolona in obrni:
                        tabla[obrni_red][obrni_kolona] = boja
                    break


def validni_potezi(tabla, boja):
    # Dobavljanje svih mogucih poteza za prosledjenu boju
    svi_validni_potezi = []
    for red in range(SIZE):
        for kolona in range(SIZE):
            if validan_potez(tabla, red, kolona, boja):
                svi_validni_potezi.append((red, kolona))
    return svi_validni_potezi

def ispisi_validne_poteze(tabla, boja):
    svi_validni_potezi = validni_potezi(tabla, boja)
    if len(svi_validni_potezi) == 0:
        print("Nema mogucih poteza za " + boja)
    else:
        print("Moguci potezi: ")
        for potez in svi_validni_potezi:
            print("Red: " + str(potez[0]), "Kolona: " + str(potez[1]))