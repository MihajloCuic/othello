from konstante import *

def proveri_rezultat(tabla, boja):
    #Dobavljanje broja plocica za prosledjenu boju
    rezultat = 0
    for red in range(SIZE):
        for kolona in range(SIZE):
            if tabla[red][kolona] == boja:
                rezultat += 1
    return rezultat


def stanje(tabla, boja):
    #Procena stanja ploce
    rezultat = 0
    for red in range(SIZE):
        for kolona in range(SIZE):
            if tabla[red][kolona] == boja:
                rezultat += heuristika[(red, kolona)]
    return rezultat