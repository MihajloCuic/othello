from konstante import *

def napravi_tablu():
    # Kreiranje pocetne ploce
    tabla = [[PRAZNO] * SIZE for _ in range(SIZE)]
    tabla[3][3] = BELI
    tabla[3][4] = CRNI
    tabla[4][3] = CRNI
    tabla[4][4] = BELI
    return tabla


def iscrtaj_tablu(tabla):
    # Ispisivanje trenutne ploce
    print("  ", end="")
    for kolona in range(SIZE):
        print(kolona, end=" ")
    print()
    for red in range(SIZE):
        print(red, end=" ")
        for kolona in range(SIZE):
            if tabla[red][kolona] == PRAZNO:
                print(".", end=" ")
            elif tabla[red][kolona] == CRNI:
                print("B", end=" ")
            else:
                print("W", end=" ")
        print()